class CreateClassDays < ActiveRecord::Migration
  def change
    create_table :class_days do |t|
      t.string :day

      t.timestamps null: false
    end
  end
end
