class AddStudentIdToParentInformation < ActiveRecord::Migration
  def change
    change_table :student_parent_informations do |t|
      t.belongs_to :student
    end
  end
end
