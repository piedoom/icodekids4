class CreateCourseUsers < ActiveRecord::Migration
  def change
    create_table :course_students do |t|
      t.belongs_to :student
      t.belongs_to :course
      t.belongs_to :classtime
      t.timestamps null: false
    end
  end
end
