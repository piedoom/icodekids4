class CreateStudentParentInformations < ActiveRecord::Migration
  def change
    create_table :student_parent_informations do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :homephone
      t.string :cellphone
      t.string :workphone

      t.timestamps null: false
    end
  end
end
