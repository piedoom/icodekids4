class CreateClasstimes < ActiveRecord::Migration
  def change
    create_table :weekdays do |t|
          t.string :day
          t.timestamps null: true
    end

    create_table :classtimes do |t|
      t.belongs_to :weekday
      t.belongs_to :course
      t.string :start_time
      t.string :end_time
      t.timestamps null: false
    end
  end
end
