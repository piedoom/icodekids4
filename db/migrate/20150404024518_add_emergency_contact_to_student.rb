class AddEmergencyContactToStudent < ActiveRecord::Migration
  def change
    change_table :students do |t|
      t.string :emergency_contact_first_name
      t.string :emergency_contact_last_name
      t.string :emergency_contact_phone
    end
  end
end
