class Letstudentowntimes < ActiveRecord::Migration
  def change
    change_table :students do |t|
      t.belongs_to :class_time
      t.belongs_to :class_day
    end
  end
end
