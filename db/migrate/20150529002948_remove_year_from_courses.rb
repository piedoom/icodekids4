class RemoveYearFromCourses < ActiveRecord::Migration
  def change
    remove_column :courses, :year
  end
end
