class Addmoretostudents < ActiveRecord::Migration
  def change
    change_table :students do |t|
      t.text :medical_conditions
      t.boolean :community_consent, :default => :false
      t.boolean :photograph_consent, :default => :false
      t.boolean :pickup_consent, :default => :false
      t.string :door_code, :default => nil
      t.date :start_date
    end
  end
end
