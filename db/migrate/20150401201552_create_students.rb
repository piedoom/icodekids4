class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :firstname, null: false
      t.string :lastname, null: false
      #gender will be m, f, or o for other
      t.string :gender
      t.date :birthday, null: false
      #the user id the student refers to.
      #this will be blank if a parent creates the student, and will be filled in later by the student
      #this will have an integer corresponding to the user_id if the user is enrolling themself
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end
