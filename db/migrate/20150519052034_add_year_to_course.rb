class AddYearToCourse < ActiveRecord::Migration
  def change
    #add year (date) to course when supplied.  Good for filtering, so old obsolete classes are not shown.  if left nil, the class is endless and is available every year.
    add_column :courses, :year, :integer, default: nil
  end
end
