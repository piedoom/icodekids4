module ApplicationHelper
  def user_or_email
    if user_signed_in?
      if current_user.username != nil
        return current_user.username
      else
        return current_user.email
      end
    end
  end

  def user_is_admin?
    if user_signed_in?
      if current_user.admin
        return true
      else
        return false
      end
    end
  end

  def student_full_name(student)
    "#{student.firstname.capitalize} #{student.lastname.capitalize}"
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end




end
