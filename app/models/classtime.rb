class Classtime < ActiveRecord::Base

  TIMES = ['1:00','1:30','2:00','2:30','3:00','3:30','4:00','4:30','5:00','5:30','6:00','6:30','7:00']

  belongs_to :course
  belongs_to :weekday
  has_many :course_students
end
