class Course < ActiveRecord::Base

  YEARS = []
  6.times do |yearcount|
    YEARS << Time.now.year + yearcount
  end
  #this allows the user to select the year when the course is available.  it looks 6 years into the future for planning purposes.  if nil, the course is offered every year


  monetize :price_cents #allow this column to hold prices

  belongs_to :course_type #difines if class is summer, school, vacation, etc.
  has_many :classtimes #has an array of all possible classtimes

  has_many :students, :through => :course_students #list of students in this course
  has_many :course_students

  accepts_nested_attributes_for :classtimes, :reject_if => :all_blank, :allow_destroy => true

  validates :name, :code, :course_type_id, :presence => true

end
