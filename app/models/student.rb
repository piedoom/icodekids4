class Student < ActiveRecord::Base

  GENDERS = ["Male", "Female", "Other"]


  belongs_to :user
  belongs_to :class_day
  belongs_to :class_time

  has_many :courses, :through => :course_students
  has_many :course_students

  has_many :student_parent_informations, :dependent => :destroy
  accepts_nested_attributes_for :student_parent_informations, :allow_destroy => true

  validates :firstname, :lastname, :birthday, :user_id,
      presence: true

  validate :require_parents

  #TODO: validate everything


  #function to set the user

  def set_user!(user)
    self.user_id = user.id
    self.save!
  end

  private

  def require_parents
    errors.add(:base, "You must provide at least one parent.") if student_parent_informations.size < 1 #TODO: validate when deleting with JS
  end


end
