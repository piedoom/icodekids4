class Image < ActiveRecord::Base
  has_attached_file :file, styles: {large: "1400x"}
  validates_attachment_content_type :file, content_type: /\Aimage\/.*\Z/
end
