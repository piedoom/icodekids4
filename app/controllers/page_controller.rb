class PageController < ApplicationController
  def home
    @contents = Content.all
    renderer = Redcarpet::Render::HTML.new(render_options = {})
    @markdown = Redcarpet::Markdown.new(renderer, extensions = {})
  end

  def about
  end

  def contact
  end

  def jcc
  end

  def academic
  end

  def camps
  end

  def events
  end

  def curriculum
  end

  def courses
    @courses = Course.all
  end
end
