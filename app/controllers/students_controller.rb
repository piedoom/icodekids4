class StudentsController < ApplicationController
  before_action :set_student, only: [:update, :destroy]

  # GET /students
  # GET /students.json
  def index
    if user_is_admin?
      @students = Student.order(:lastname)
      respond_to do |format|
        format.html
        format.csv
      end
    else
      redirect_to root_path
    end
  end

  # GET /students/1
  # GET /students/1.json
  def show
    if @student = Student.find_by_id(params[:id])
      student_user_verify(@student,true)
    else
      redirect_to root_path
    end
  end

  # GET /students/new
  def new
    #check if logged in
    if user_signed_in?
      @student = Student.new
      2.times { @student.student_parent_informations.build } #TODO: fix this
    else
      respond_to do |format|
        format.html { redirect_to root_path, notice: 'Must be logged in to sign up a new student.' }
      end
    end
  end

  # GET /students/1/edit
  def edit
    if @student = Student.find_by_id(params[:id])
      student_user_verify(@student,true)
    else
      redirect_to root_path
    end
  end

  # POST /students
  # POST /students.json
  def create
    if user_signed_in?
      @student = Student.new(student_params)
      @student.set_user!(current_user)

      respond_to do |format|
        if @student.save
          format.html { redirect_to @student, notice: 'Student was successfully created.' }
          format.json { render :show, status: :created, location: @student }
        else
          format.html { render :new }
          format.json { render json: @student.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    if student_user_verify(@student,false)
      respond_to do |format|
        if @student.update(student_params)
          format.html { redirect_to @student, notice: 'Student was successfully updated.' }
          format.json { render :show, status: :ok, location: @student }
        else
          format.html { render :edit }
          format.json { render json: @student.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to(root_path)
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    if student_user_verify(@student,false)
      @student.destroy
      respond_to do |format|
        format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to root_path
    end
  end

  def courses
    @student = Student.find_by_id(params[:id])
    student_user_verify(@student,true)
    @courses = @student.courses #.where("year >= ? OR year IS NULL", Time.now.year  )
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_student
    @student = Student.find(params[:id])
  end

  def student_user_verify(student,redirect)
    if student = Student.find_by_id(params[:id])
      if (user_signed_in? && student.user_id == current_user.id) || user_is_admin?
        allowed = true
        return allowed
      else
        allowed = false
        if redirect
          redirect_to root_path
        else
          return allowed
        end
      end
    elsif redirect
      redirect_to root_path
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def student_params
    params.require(:student).permit(:firstname, :lastname, :birthday, :class_time_id, :class_day_id, :emergency_contact_first_name, :emergency_contact_last_name, :emergency_contact_phone, :medical_conditions, :community_consent, :photograph_consent, :pickup_consent, :door_code, :start_date, :gender,
                                    :course_ids => [],
                                    student_parent_informations_attributes: [:id, :_destroy, :firstname, :lastname, :email, :address, :city, :state, :zip, :homephone, :cellphone, :workphone])
  end
end
