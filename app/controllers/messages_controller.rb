class MessagesController < ApplicationController

  def new
    @message = Message.new
  end

  def create
    #if verify_recaptcha TODO: fix recaptcha
      @message = Message.new(message_params)

      if @message.valid?
        MessageMailer.delay.new_message(@message) #.send_later(:deliver)
        redirect_to contact_path, notice: "Your messages has been sent."
      else
        flash[:alert] = "An error occurred while delivering this message."
        render :new
      end
    #else
    #  flash[:alert] = "An error occurred while delivering this message."
    #  redirect_to contact_path, notice: "Your message was not sent.  Please complete the security verification."
    #end
  end

  private

  def message_params
    params.require(:message).permit(:name, :email, :content, :phone_number, :referral)
  end

end