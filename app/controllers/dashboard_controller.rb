class DashboardController < ApplicationController
  def index

    if user_is_admin?
      redirect_to admin_path
    end
  end

  def admin
    user_is_admin? ? nil : (redirect_to root_path)
    @contents = [Content.find_or_create_by(title: 'promos')]
  end
end
