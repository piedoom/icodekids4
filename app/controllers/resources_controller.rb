class ResourcesController < ApplicationController
  def dragdrop
  end

  def completetag
  end

  def editor
  end

  def download
    @data = params[:filedata]
    @dataName = params[:downloadName]
    send_data @data, filename: @dataName + ".html"
  end

end
