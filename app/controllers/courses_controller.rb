class CoursesController < ApplicationController
  include ApplicationHelper
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.where("year >= ? OR year IS NULL", Time.now.year  )
    respond_to do |format|
      format.html
      format.csv { render csv: @courses }
    end
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
  end

  def search
  end

  # GET /courses/new
  def new
    if user_is_admin?
      @course = Course.new
      @course.classtimes.build
    else
      redirect_to root_path
    end
  end

  # GET /courses/1/edit
  def edit
    if !user_is_admin?
      redirect_to root_path
    end
  end

  # POST /courses
  # POST /courses.json
  def create
    if user_is_admin?
      @course = Course.new(course_params)

      respond_to do |format|
        if @course.save
          format.html { redirect_to @course, notice: "#{@course.name} was successfully created." }
          format.json { render :show, status: :created, location: @course }
        else
          format.html { render :new }
          format.json { render json: @course.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    if user_is_admin?
      respond_to do |format|
        if @course.update(course_params)
          format.html { redirect_to @course, notice: "#{@course.name} was successfully updated." }
          format.json { render :show, status: :ok, location: @course }
        else
          format.html { render :edit }
          format.json { render json: @course.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    if user_is_admin?
      @course.destroy
      respond_to do |format|
        format.html { redirect_to courses_url, notice: "#{@course.name} was successfully deleted." }
        format.json { head :no_content }
      end
    else
      redirect_to root_path
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :code, :description, :duration, :course_type_id, :price, classtimes_attributes: [:id, :start_time, :end_time, :weekday_id, :year, :_destroy])
    end
end
