class MessageMailer < ActionMailer::Base

  default from: "iCanCode Bot <info@icancodeclub.com>"
  default to: ["underscoreseventy@gmail.com"]

  def new_message(message)
    @message = message
    mail(subject: "Website Message from #{message.name}", to: "info@icancodeclub.com", template_name: 'admin_new_message').deliver!
    mail(subject: "Hi #{message.name}, Here's a confirmation of the email you just sent us.", to: message.email).deliver!
  end

end
