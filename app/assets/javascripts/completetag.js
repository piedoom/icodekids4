$(document).ready(ready);
$(document).on('page:load', ready);

//possible tags to be used for the solveFor
taglist = ['head','body','html','p','h1','section','footer','article','ul','li'];

function ready(){
  function error(){
    //errors and help
    console.log('nope');
  }

  function checkAnswer(answer){
    section = focusWork;
    console.log(cv[section]);
    if (answer == cv[section]){
      //very non-dry section of code...
      //moves on to next section or completes card if none exists
      if (section == 0){
        //move to second section
        cardSelect($('.tcb'));
        focusWork = 1;
      }
      if (section == 1){
        //move to third section
        cardSelect($('.tcc'));
        focusWork = 2;
      }
      if (section == 2){
        //move to new card
        focusWork = 0;
        newCard();
      }

    }else{
      console.log('oops');
    }
  }

  function cardSelect(card){
    //disabling everything but the first input
    $(".tcinput").find('input').prop('disabled', true);
    card.find('input').prop('disabled', false);
    card.find('input').focus();
  }

  function newCard(){
    //decide whether to make student fill in top or bottom section
    position = Math.floor(Math.random() * 2) + 1;
    console.log(position);

    //create puzzle target and puzzle
    //position of 1 means user starts tag, 2 ends tag
    if (position == 1){
      target = $('.tcfirst');
      solveFor = $('.tcsecond');
    }else{
      target = $('.tcsecond');
      solveFor = $('.tcfirst');
    }

    //set target to editable
    target.find('.tcinput').html('<input type="text"/>');

    //figure if the solveFor is beginning or ending
    //add to an array called "cv"
    cv = ["","",""]
    if (position == 1){
      cv[0] = '<';
      solveFor.find('.tca').text('</');
    }else{
      cv[0] = '</';
      solveFor.find('.tca').text('<');
    }
    //this part is always the same
    cv[2] = '>';
    solveFor.find('.tcc').text(cv[2]);
    //select a random tag
    tagIndex = Math.floor(Math.random() * taglist.length);
    tag = taglist[tagIndex];
    console.log(tag);
    //insert that tag into the puzzle.
    cv[1] = tag;
    solveFor.find('.tcb').text(cv[1]);
    //we now have an array with the correct values.  let's make it global
    if (window[cv] !== undefined){
      console.log('cleared gloabl');
      delete window.cv;
    }
    window.cv = cv;
    console.log(cv);

    ////////////////////////////////////////////////
    /////////// getting user input /////////////////
    ////////////////////////////////////////////////


    //undisable the first one
    //focus on the first item.  We're using a function so we can display relevant text.
    cardSelect($('.tca'));

    ////////////////////////////////////////////////
    /////////// setting help text //////////////////
    ////////////////////////////////////////////////
    help = $('#tagcompletehelp');
    helperheader = position == 1 ? 'Begin the ' : 'End the ';
    help.find('h1').text(helperheader + cv[1] + ' tag' );



  }
  newCard();


  //setting which form the student is working on
  focusWork = 0;

  //if the enter key is pressed
  $(document).keypress(function(e) {
    if(e.which == 13) {
      //check which the student is working on
      if (focusWork == 0){
        checkAnswer($('.tca').find('input').val());
      }else
      if (focusWork == 1){
        checkAnswer($('.tcb').find('input').val());
      }else
      if (focusWork == 2){
        checkAnswer($('.tcc').find('input').val());
      }else
      {

      }
    }
  });

};
