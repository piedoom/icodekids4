$(document).ready(ready);
$(document).on('page:load', ready);

function dialog(dialog){
  $(dialog).toggleClass('visible');
}

function ready(){
  var editor = ace.edit("editor");
  editor.setTheme("ace/theme/twilight");
  editor.getSession().setMode("ace/mode/html");
  //setting font size
  editor.setFontSize('20px');

  //resizing the editor
  $("#htmledit").resizable();

  //updating the
  //probably a better way with the api
  editor.on("change", function(){
    var code = editor.getSession().getValue();
    //printing code to html
    $('#htmlview').html(code);
    $('#htmlcontent').val(code);
  });

  $("#downloadButton").click(function(){
    dialog('#dialogDownload')
  });

  $(window).resize(function(){
    allResizeFunctionsEditor();
  });

  function allResizeFunctionsEditor(){
    belowItem('#dialogDownload','#downloadButton','#topbar')
  }

}
