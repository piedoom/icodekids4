/*file for universal.*/
$(document).ready(ready);
$(document).on('page:load', ready);

function ready(){
  logoHeader = $('#logoContainer');
  navHeader = $('nav');
  headerHeader = $('header');
  navLogin = $('#navLogin');

  //bind click event to all anchors with ids
  $("a[href*=#]").bind("click", function(e) {
    // prevent default action and bubbling
    e.preventDefault();
    e.stopPropagation();
    // set target to anchor's "href" attribute
    var target = $(this).attr("href");
    // scroll to each target
    $(target).velocity("scroll", {
      duration: 700,
      offset: -10,
      easing: "ease-in-out"
    });
  });

  /*called from resize*/
  function updateHeader(){
    navHeader.width(headerHeader.width() - logoHeader.width() - navLogin.outerWidth() );
  }

  /*reusable functions*/
  function centerVertical(item,parent){
    //resetting variables for ease
    item = $(item);
    parent = $(parent);
    item.css('margin-top',parent.outerHeight()/2 - item.outerHeight()/2)
  }

  function centerRelativeVertical(item,parent){
    //resetting variables for ease
    item = $(item);
    parent = $(parent);
    item.css('top',parent.outerHeight()/2 - item.outerHeight()/2 - $('nav').height());
  }

  function fullHeight(item,windowHeight){
    item = $(item);
    item.css('height',windowHeight)
  }

  function sameHeight(item,parent){
    item = $(item);
    ph = $(parent);
    item.css('height',ph.height());
  }

  function veloPopUp(item){
    $(item).velocity("transition.slideUpIn", 1250);
  }

  /* functions to create awesome transtitions */
  function fadeUpIn(item){
    $(item).velocity("transistion.slideUpIn",1250);
  }

  function fitWidth(item1, item2, parent){
    $(item2).width($(parent).width()-$(item1).width());
  }

  allResizeFunctions();
  veloPopUp('.veloPopUp');

  /* 'when visible' functions */
  $('.wow').waypoint(function() {
    if (!$(this).hasClass('triggered')){
      if ($(this).hasClass('popUp')){
        $(this).velocity("transition.slideUpIn", 1000);
      }
      if ($(this).hasClass('slideRightIn')){
        $(this).velocity("transition.slideRightIn", 1000);
      }
      if ($(this).hasClass('slideLeftIn')){
        $(this).velocity("transition.slideLeftIn", 1000);
      }
      if ($(this).hasClass('expandIn')){
        $(this).velocity("transition.expandIn", 1000);
      }
      if ($(this).hasClass('fadeIn')){
        $(this).velocity("transition.fadeIn", 1000);
      }

      $(this).addClass('triggered');
    }
  },{ offset: '94%' });

  $('hr').waypoint(function(){
    if (!$(this).hasClass('triggered')){
      $(this).velocity({width: '100%'},{duration: 1000})
      $(this).addClass('triggered');
    }
  },{ offset: '90%' });

  $(window).resize(function(){
    allResizeFunctions();
  });

  function allResizeFunctions(){
    //only call window height once and pass it to multiple functions
    wh = $(window).height();
    h = $('header').outerHeight()
    /*here*/

    fullHeight('#backgroundFull',wh-h);
    fullHeight('.app',wh-h);
    fullHeight('.fullHeight',wh-h);
    sameHeight('#backgroundCover','#backgroundFull')

    sameHeight('.backgroundImage','.backgroundImageAfter');
    sameHeight('#push','header');
    fitWidth('.leftCol','.rightCol','.colContainer');
    fitWidth('.colResponsiveSet','.colResponsive','.colContainer');


    centerVertical('nav','header')
    centerVertical('#tagcomplete','#tagcompletewrapper')
    centerRelativeVertical('#homeText','#backgroundFull')


  }



  /* functions for drag and drop app */
  /*
  */

  $('.ddelement').draggable({
    containment: ".colContainer",
    scroll: false,
    cursor: "move",
    revert: "invalid",
    stack: ".ddelement",
    grid: [ 10, 10 ]
    });

    $('.ddelement').mouseout(function( event ){
      event.preventDefault();
    });

    $("#ddaccept").droppable({
    tolerance: "intersect",
    accept: "#ddhtml",
    activeClass: "dd-state-default",
    hoverClass: "dd-state-hover",
    drop: function(event,ui){
      var offset = ui.helper.offset();
      console.log(offset);
      ui.helper.appendTo( this ).offset( offset );
      }
    });

    $("#ddhtml>.ddinside>#ddheadinside").droppable({
      tolerance: "intersect",
      accept: "#ddhead",
      activeClass: "dd-state-default",
      hoverClass: "dd-state-hover",
      drop: function(event,ui){
        var offset = ui.helper.offset();
        ui.helper.appendTo( this ).offset( offset );
      }
    });

    $("#ddhtml>.ddinside>#ddbodyinside").droppable({
      tolerance: "intersect",
      accept: "#ddbody",
      activeClass: "dd-state-default",
      hoverClass: "dd-state-hover",
      drop: function(event,ui){
        var offset = ui.helper.offset();
        ui.helper.appendTo( this ).offset( offset );
      }
    });

    $("#ddbody>.ddinside").droppable({
      tolerance: "intersect",
      accept: "#ddcontent",
      activeClass: "dd-state-default",
      hoverClass: "dd-state-hover",
      drop: function(event,ui){
        var offset = ui.helper.offset();
        ui.helper.appendTo( this ).offset( offset );
      }
    });

  /*
  */
};
