Rails.application.routes.draw do

  resources :images

  resources :news

  # set a route to match the hackathon form

  get "/other_events" => redirect("https://docs.google.com/forms/d/1DS7oHuNCYChtcR0WMxuogjYcXEkBCbkeKKf-l4VAfzQ/edit?ts=583e546b"), :as => :other_events
  get "/victor" => redirect("https://icancodeclub.mypaysimple.com/s/icancode-club-school-courses"), :as => :victor
  get "/hackathon" => redirect("https://docs.google.com/forms/d/e/1FAIpQLSd8cA-Oa_p7nbt9TkwZtP7ijqbp5VFgervPen1IqaF1qXox_Q/viewform?c=0&w=1"), :as => :hackathon
  resources :contents
  
  resources :courses

  #resources :students

  devise_for :users

  get 'resources/dragdrop'

  get 'resources/completetag'

  get 'resources/editor'

  post 'resources/download'

  get 'home' => 'page#home'
  get 'jcc' => 'page#jcc'

  get 'curriculum' => 'page#curriculum'

  get 'contact', to: 'messages#new', as: 'contact'
  post 'contact', to: 'messages#create'

  get 'courses_overview' => 'page#courses'

  get 'academic' => 'page#academic'

  get 'camps' => 'page#camps'
  get 'summer' => 'page#summer'
  get 'events' => 'page#events'

  get 'dashboard' => 'dashboard#index'

  get 'admin' => 'dashboard#admin'

  root 'page#home'
end
